# Deep ArUco

This project belongs to a master thesis that is a part of the study in master of mechatronics at the [University of Agder](https://www.uia.no/en), Campus Grimstad. The project is called Deep ArUco: AI/ML-based Real-Time Marker Pose Tracking. 

Unity is used to simulate real-life scenarios and generate data consisting of images with ArUco markers in. The detection algorithm is then used to detect the markers with different difficulties and interruptions in the scene. 

## Start the Project

Go to [Unity](https://unity.com/) and download the software. 

The project can be opened when Unity is downloaded. In the zipped file under "Unity envorinments", the environment created for this project in Unity is to be found. The datasets are created in Unity and can be found here together with all of the parameters that can be adjusted directly in Unity. 

The scripts in C# can be found both in the zipped file, but also under "Scripts". The scripts for the objects in Unity are created in [Rider](https://www.jetbrains.com/rider/).

The Python scripts are created and run in [PyCharm](https://www.jetbrains.com/pycharm/). The scripts that uses Python are the one for generating the ArUco markers, and the DeepTag algrithm used for the detection of the markers. The detection algorithm can be found her: https://github.com/herohuyongtao/deeptag-pytorch.
