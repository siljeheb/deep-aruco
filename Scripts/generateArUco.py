#Written by Silje Wetrhus Hebnes on 23.01.2022

import cv2
import numpy

#Generating ArUco markers:
    
#Loading dictionary for markers
dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
    
#Generating the ArUco marker
markerImage = numpy.zeros((200, 200), dtype=numpy.uint8)
markerImage = cv2.aruco.drawMarker(dictionary, 20, 200, markerImage, 1);
cv2.imwrite("marker20.png", markerImage);
    

  
