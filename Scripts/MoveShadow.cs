// Written by Silje Wetrhus Hebnes on 02.02.2022

using UnityEngine;

public class MoveShadow : MonoBehaviour
{
	public Vector3 framep;

    private Parameters frameParms = new Parameters();
    private int frame = 0;

    void Update()
    {
        Debug.Log("Shadow frame no: " + frame);
		//Getting parameters from list in "CameraCapture.cs":
        frameParms = GameObject.Find("MainCamera").GetComponent<CameraCapture>().parameterList[frame];
		//Changing the position of the shadow-plane:
        transform.position = new Vector3(frameParms.posx_shadow, frameParms.posy_shadow, frameParms.posz_shadow);
		//Changing the rotation of the shadow-plane:
        framep = new Vector3(frameParms.orix_shadow, frameParms.oriy_shadow, frameParms.oriz_shadow);
		UnityEditor.TransformUtils.SetInspectorRotation(transform, framep);
        frame++;
    }
}



