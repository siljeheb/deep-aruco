// Written by Silje Wetrhus Hebnes on 02.02.2022

using UnityEngine;

public class MoveAruco : MonoBehaviour
{
	public Vector3 framep;

    private Parameters frameParms = new Parameters();
    private int frame = 0;

    void Update()
    {
        Debug.Log("Shadow frame no: " + frame);
		//Getting parameters from list in "CameraCapture.cs":
        frameParms = GameObject.Find("MainCamera").GetComponent<CameraCapture>().parameterList[frame];
		//Changing the position of the ArUco marker:
        transform.position = new Vector3(frameParms.posx_aruco, frameParms.posy_aruco, frameParms.posz_aruco);
		//Changing the rotation of the ArUco marker:
		framep = new Vector3(frameParms.orix_aruco, frameParms.oriy_aruco, frameParms.oriz_aruco);
		UnityEditor.TransformUtils.SetInspectorRotation(transform, framep);
        frame++;
    }
}


