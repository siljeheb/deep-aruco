// Written by Silje Wetrhus Hebnes on 26.01.2022

using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System;
using Random = System.Random;

[System.Serializable]
public class Parameters         //Creating parameter class
{
    public float posx_aruco;    //Position x-direction for ArUco
    public float posy_aruco;    //Position y-direction for ArUco
    public float posz_aruco;    //Position z-direction for ArUco
    public float orix_aruco;    //Rotation x-direction for ArUco
    public float oriy_aruco;    //Rotation y-direction for ArUco
    public float oriz_aruco;    //Rotation z-direction for ArUco
    public float posx_camera;   //Position x-direction for Camera
    public float posy_camera;   //Position y-direction for Camera
    public float posz_camera;   //Position z-direction for Camera
    public float orix_camera;   //Rotation x-direction for Camera
    public float oriy_camera;   //Rotation y-direction for Camera
    public float oriz_camera;   //Rotation z-direction for Camera
    public float posx_shadow;   //Position x-direction for Shadow-plane
    public float posy_shadow;   //Position y-direction for Shadow-plane
    public float posz_shadow;   //Position z-direction for Shadow-plane
    public float orix_shadow;   //Rotation x-direction for Shadow-plane
    public float oriy_shadow;   //Rotation y-direction for Shadow-plane
    public float oriz_shadow;   //Rotation z-direction for Shadow-plane
    public float brightness;    //Brightness in Unity
}

public class CameraCapture : MonoBehaviour
{
    public List<Parameters> parameterList = new List<Parameters>();     //List of parameters
    public SnapshotCamera snapCam;
    
    //Range of variables for data generation:
    private int posy_aruco_min = 10;
    private int posy_aruco_max = 450;
    private int orix_aruco_min = 0;
    private int orix_aruco_max = 89;
    private int oriy_aruco_min = 0;
    private int oriy_aruco_max = 359;
    private int oriz_aruco_min = 0;
    private int oriz_aruco_max = 89;
    private int posx_shadow_min = -10;
    private int posx_shadow_max = 10;
    private int posz_shadow_min = -10;
    private int posz_shadow_max = 10;
    private int orix_shadow_min = 0;
    private int orix_shadow_max = 89;
    private int oriy_shadow_min = 0;
    private int oriy_shadow_max = 359;
    private int oriz_shadow_min = 0;
    private int oriz_shadow_max = 89;
    
  
    private int frame = 0;
    
    //Path for images:
    private string imageFolderName = "CameraImages";
    private string imagePath;
    
    //Path for json file:
    private string jsonFolderName = "jsonFolder";
    private string jsonPath;
    
    //Starting the script
    void Start()
    {
        Application.targetFrameRate = 2;
        Random r = new Random();
        jsonPath = Path.Combine(Environment.CurrentDirectory, jsonFolderName);
        Directory.CreateDirectory(jsonPath);

        Debug.Log("Starting screen capture script..");
        
        for (int i = 0; i < 1000; i++)      //Changing the parameters and creating the json file 1000 times
        {
            parameterList.Add(new Parameters()
            {
                posx_aruco = 0,
                posy_aruco = (r.Next(posy_aruco_min, posy_aruco_max)),
                posz_aruco = 0,
                orix_aruco = (r.Next(orix_aruco_min, orix_aruco_max)),
                oriy_aruco = (r.Next(oriy_aruco_min, oriy_aruco_max)),
                oriz_aruco = (r.Next(oriz_aruco_min, oriz_aruco_max)),
                posx_camera = 0,
                posy_camera = 500,
                posz_camera = 0,
                orix_camera = 90,
                oriy_camera = 0,
                oriz_camera = 0,
                posx_shadow = (r.Next(posx_shadow_min, posx_shadow_max)),
                posy_shadow = 550,
                posz_shadow = (r.Next(posz_shadow_min, posz_shadow_max)),
                orix_shadow = (r.Next(orix_shadow_min, orix_shadow_max)),
                oriy_shadow = (r.Next(oriy_shadow_min, oriy_shadow_max)),
                oriz_shadow = (r.Next(oriz_shadow_min, oriz_shadow_max)),
                brightness = 0
            });
            
            // save current object to a file
            string jsonString = JsonUtility.ToJson(parameterList[i]);
            string jsonFilePath = Path.Combine( jsonPath, "jsonFile_" + i + ".json");
            File.WriteAllText(jsonFilePath, jsonString, Encoding.UTF8);
        }
        Debug.Log("Parameters complete ");
    }

    private void Update()
    {
        //Gets the screenshots from "SnapshotCamera.cs" and updates them. 
        snapCam.CallTakeSnapshot();
        Debug.Log("UPDATE frame no: " + frame);
        frame++;
    }
    
    void LateUpdate()
    {
        //Updating 1000 times. 
        if (frame >= 1000)
        {
            UnityEditor.EditorApplication.ExitPlaymode();
        }
    }
}



