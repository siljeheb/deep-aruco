// Written by Silje Wetrhus Hebnes on 10.03.2022

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System;
using Random = System.Random;

[RequireComponent(typeof(Camera))]
public class SnapshotCamera : MonoBehaviour
{
	//Variables:
    private Camera snapCam;
    
    public int resWidth = 256; 
    public int resHeight = 256;
    
    private string imageFolderName = "CameraImages";
    private string imagePath;
    private int frame = 0;
    
    private bool takeHiResShot = false;

    private void Awake()
    {
        //Creating image path:
        imagePath = Path.Combine(Environment.CurrentDirectory, imageFolderName);
        Directory.CreateDirectory(imagePath);
        
        snapCam = GetComponent<Camera>();
        if (snapCam.targetTexture == null)
        {
            snapCam.targetTexture = new RenderTexture(resWidth, resHeight, 24);
        }
        else
        {
            resWidth = snapCam.targetTexture.width;
            resHeight = snapCam.targetTexture.height;
        }
        snapCam.gameObject.SetActive(false);
    }

    
    public void CallTakeSnapshot()
    {
        snapCam.gameObject.SetActive(true);
    }
 
    void LateUpdate() {
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        snapCam.Render();
        RenderTexture.active = snapCam.targetTexture; // JC: added to avoid errors
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        byte[] bytes = screenShot.EncodeToPNG();
        string imageFilePath = Path.Combine(imagePath, "ScreenShot_" + frame + ".png");
        File.WriteAllBytes(imageFilePath, bytes);
        Debug.Log("Snapshot taken: " + frame);
        frame++;
        snapCam.gameObject.SetActive(false);
    }
}


